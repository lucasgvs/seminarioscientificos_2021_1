package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class AreaCientifica {

    private List<Curso> cursos = new ArrayList<>();
    private Long id;
    private String nome;

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);

    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);

    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public Long getId() {
        return this.id;
    }

}
