package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private LocalDate data;
    private Integer qtdInscricoes;

    public Seminario(AreaCientifica area, Professor professor, Integer qtdInscricoes) {
        this.adicionarProfessor(professor);
        professor.adicionarSeminario(this);
        this.adiconarAreaCientifica(area);
        this.qtdInscricoes = qtdInscricoes;

        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public void adiconarAreaCientifica(AreaCientifica area) {
        this.areasCientificas.add(area);

    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);

    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);

    }

    public boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areasCientificas.contains(area);

    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);

    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public java.time.LocalDate getData() {
        return this.data;
    }

    public void setData(java.time.LocalDate data) {
        this.data = data;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public Long getId() {
        return this.id;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

}
